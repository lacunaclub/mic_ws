# web audio microphone streaming to raspberry pi speakerz


setup
```
sudo apt-get install node npm
sudo npm install -g bower
npm install
bower install
```

ssl (optional)
```
openssl req -new -x509 -keyout key.pem -out cert.pem -days 666 -nodes
```

run socket server
```
node server.js  
-or-
npm start
-or-
./mic_ws.sh
```

run web server
```
python -m SimpleHTTPServer 8000
-or-
sudo python -m SimpleHTTPServer 80
```
-or-
...whatever port(z) you like!
